﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadingMVVMWPF
{
    public static class URLFileReader
    {
        public static async Task<List<string>> GetURLListAsync(string path)
        {
            var urls = new List<string>();
            string s = await ReadURLFileAsync(path);
            urls.AddRange(s.Split('\n'));
            return urls;
        }

        private static async Task<string> ReadURLFileAsync(string path)
        {
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(path))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = await sr.ReadToEndAsync();
                    return line;
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                return e.Message;
            }
        }
    }
}
