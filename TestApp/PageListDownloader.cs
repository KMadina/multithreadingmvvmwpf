﻿using MultiThreadingMVVMWPF.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace MultiThreadingMVVMWPF
{
    static class PageListDownloader
    {
        public static async Task<List<URLContentInfo>> DownloadPageListAsync(string path)
        {
            //await Task.Delay(5000);
            var urlList = await URLFileReader.GetURLListAsync(path);

            List<URLContentInfo> htmlList = new List<URLContentInfo>();
            foreach (var url in urlList)
            {
                string urlContent = await DownloadHTMLAsync(url);

                htmlList.Add(new URLContentInfo(url, urlContent));
            }
            return htmlList;
        }



        static async Task<string> DownloadHTMLAsync(string url)
        {
            using (WebClient client = new WebClient())
            {
                client.Credentials = CredentialCache.DefaultNetworkCredentials;

                string s = await client.DownloadStringTaskAsync(url);

                return s;
            }
        }
    }
}
