﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadingMVVMWPF.Model
{
    class URLContentInfo
    {
        public string URL;
        public string content;

        public URLContentInfo(string URL, string content)
        {
            this.URL = URL;
            this.content = content;
        }
    }
}
