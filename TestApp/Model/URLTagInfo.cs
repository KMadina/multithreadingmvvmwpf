﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadingMVVMWPF
{
    class URLTagInfo
    {
        string URL;
        int tagCount;

        public URLTagInfo(string URL, int tagCount)
        {
            this.URL = URL;
            this.tagCount = tagCount;
        }

        public override string ToString()
        {
            return "URL:" + URL + "\r\nNumber of tags: " + tagCount.ToString()+"\r\n";
        }
    }
}
