﻿
using System.Windows;
using MultiThreadingMVVMWPF.ViewModel;

namespace MultiThreadingMVVMWPF
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow window = new MainWindow();
            MainWindowViewModel viewModel = new MainWindowViewModel();

            window.DataContext = viewModel;
            window.Show();
        }
    }
}
