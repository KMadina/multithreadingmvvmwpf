﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MultiThreadingMVVMWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace MultiThreadingMVVMWPF.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {
        static string FullPath => "C:\\Users\\guest\\Desktop\\URLFile.txt";
        CancellationTokenSource cts;
        string _message;
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                RaisePropertyChanged("Message");
            }
        }

        string _maxTagUrl;
        public string MaxTagUrl
        {
            get
            {
                return _maxTagUrl;
            }
            set
            {
                _maxTagUrl = value;
                RaisePropertyChanged("MaxTagUrl");
            }
        }

        List<URLContentInfo> _URLContentInfoList;
        public List<URLContentInfo> URLContentInfoList
        {
            get
            {
                return _URLContentInfoList;
            }
            set
            {
                _URLContentInfoList = value;
                RaisePropertyChanged("URLContentInfoList");
            }
        }
        
        Dictionary<string, string> _pageList;
        public Dictionary<string, string> PageList
        {
            get
            {
                return _pageList;
            }
            set
            {
                _pageList = value;
                RaisePropertyChanged("PageList");
            }
        }

        ObservableCollection<URLTagInfo> _URLInfoList;
        public ObservableCollection<URLTagInfo> URLInfoList
        {
            get
            {
                if (_URLInfoList == null)
                    _URLInfoList = new ObservableCollection<URLTagInfo>();
                return _URLInfoList;
            }
            set
            {
                _URLInfoList = value;
            }
        }


        bool _IsEnableAnimation;
        public bool IsEnableAnimation
        {
            get
            {
                return _IsEnableAnimation;
            }
            set
            {
                _IsEnableAnimation = value;
                RaisePropertyChanged("IsEnableAnimation");
            }
        }

        RelayCommand _startCountTagCommand;
        public ICommand StartCountTag
        {
            get
            {
                if (_startCountTagCommand == null)
                    _startCountTagCommand = new RelayCommand(ExecuteStartCountTagCommand);
                return _startCountTagCommand;
            }
        }

        public async void ExecuteStartCountTagCommand()
        {
            Message = null;
            IsEnableAnimation = true;
            cts = new CancellationTokenSource();

            try
            {
                if (URLContentInfoList == null)
                {
                    Message = "URL list in the process of loading.";
                    URLContentInfoList = await PageListDownloader.DownloadPageListAsync(FullPath);
                }
                
                Message = "Procedure started.";
                await CountTagsAsync(cts.Token, URLContentInfoList);

                if (!cts.Token.IsCancellationRequested)
                {
                    Message = "Procedure ended successfully.";
                }

                IsEnableAnimation = false;
            }
            catch (OperationCanceledException)
            {
                Message += "Procedure canceled.";
            }
            catch (Exception ex)
            {
                Message += "Procedure failed." + ex.Message;
            }
        }

        RelayCommand _cancelCountTagCommand;
        public ICommand CancelCountTag
        {
            get
            {
                if (_cancelCountTagCommand == null)
                    _cancelCountTagCommand = new RelayCommand(ExecuteCancelCountTagCommand);
                return _cancelCountTagCommand;
            }
        }

        public void ExecuteCancelCountTagCommand()
        {       
            if (cts != null)
            {
                cts.Cancel();
                IsEnableAnimation = false;
                Message = "Procedure canceled";
            }
            
        }

        public async Task CountTagsAsync(CancellationToken ct, List<URLContentInfo> URLContentInfoList)
        {
            var max = 0;
            var maxURL = "";
            
            foreach (var page in URLContentInfoList)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }
                else
                {
                    Task<int> countTag = CountTagAFromHtmlAsync(page.content);
                    int current = await countTag;
                    if (max < current)
                    {
                        max = current;
                        maxURL = page.URL;
                    }

                    var _URLInfo = new URLTagInfo(page.URL, current);
                    URLInfoList.Add(_URLInfo);
                }
            }
            
            if (!ct.IsCancellationRequested)
            {
                MaxTagUrl = string.Format("\nURL: {0,-20} ", maxURL);
            }
        }

        private async Task<int> CountTagAFromHtmlAsync(string s)
        {
            string s0 = "</a>";
            int count = 0;
            await Task.Run(() =>
            {
                count = ((s.Length - s.Replace(s0, "").Length) / s0.Length);
            });
            await Task.Delay(1000);

            return count;
        }


    }
}
